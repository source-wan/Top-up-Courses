import math
import  random as  rd
import  string

basestring=['赵','钱','孙','李']
namestring1=['劳孙','双都','而非','安抚']
namestring2=['劳孙','双都','而非','安抚']
def gen_email():
    suffix=['.com','org','edu.cn','.net']
    chars=string.ascii_letters+string.digits+'_'
    username=''.join((rd.choice(chars)for x in range(rd.randint(6,12))))
    domainname=''.join((rd.choice(chars)for x in range(rd.randint(3,6))))
    return username+'@' + domainname +rd.choice(suffix)
def main():
    with open ('myrandom.txt','w') as fp:
        fp.write('姓名       性别       年龄       电话号码      E-mail\n')
        for x in range(100):
            sex = rd.choice(('男','女'))
            if sex == '男':
                name=''+rd.choice(basestring)+rd.choice(namestring1)
            else:
                name=''+rd.choice(basestring)+rd.choice(namestring2)
            age = rd.randint(15,25)
            tel =''.join((str(rd.randint(0,9)) for i in range(10)))
            email = gen_email()
            me=''+name+'      '+sex+'      '+str(age)+'     '+tel+'     '+email+'\n'
            fp.write(me)
        fp.close()
    with open('myrandom.txt','r') as fp:
        while True:
            linedate=fp.readline()
            for x in linedate:
                print(x,end=' ')
if __name__ == '__main__':
    main()
